package com.example.demo.visual;

import com.example.demo.dominio.Estudiante;
import com.example.demo.dominio.EstudianteRepository;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Guicho on 15/07/2017.
 */
@SpringUI
public class App extends UI {
    @Autowired
    EstudianteRepository estudianteRepository;

    @Override
    protected void init (VaadinRequest vaadinRequest){
        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout hlayout = new HorizontalLayout();
        TextField nombre = new TextField("Nombre");
        TextField edad = new TextField("Edad");

        Grid<Estudiante> grid = new Grid<>();
        grid.addColumn(Estudiante::getId).setCaption("ID");
        grid.addColumn(Estudiante::getNombre).setCaption("Nombre");
        grid.addColumn(Estudiante::getEdad).setCaption("Edad");

        Button add = new Button("Adicionar");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Estudiante e = new Estudiante();
                e.setNombre(nombre.getValue());
                e.setEdad(Integer.parseInt(edad.getValue()));

                estudianteRepository.save(e);
                grid.setItems(estudianteRepository.findAll());

                nombre.clear();
                edad.clear();
            }
        });

        hlayout.addComponentsAndExpand(nombre,edad,add);
        hlayout.setComponentAlignment(add, Alignment.BOTTOM_RIGHT);

        layout.addComponentsAndExpand(hlayout);
        layout.addComponentsAndExpand(grid);

        setContent(layout);
    }
}
